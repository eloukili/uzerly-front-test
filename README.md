# Front end Test

## clone repository
git clone https://eloukili@bitbucket.org/eloukili/uzerly-front-test.git

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm start

# build for production with minification
npm run build
```
