import React, { Component } from 'react'
import { Helmet } from "react-helmet";
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
export default class EcommerceDashboard extends Component {
	render() {
		const { match } = this.props;
		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>Ecommerce Dashboard</title>
					<meta name="description" content="Uzerly Employers Dashboard" />
				</Helmet>
				<PageTitleBar title="Uzerly Employers" match={match} enableBreadCrumb={false}/>
				<div>
					Body of Code
				</div>

			</div>
		)
	}
}
